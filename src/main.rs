//! A command-line timer program for Ubuntu.
//!
//! This program depends on `notify-send` for sending notifications.
//!
//! Usage:
//!
//! For a default timer (20 minutes)
//!
//!     cdown
//!
//! For a specific timer
//!
//!     cdown mm:ss
//!
//! For example,
//!
//!     cdown 4:33

#[derive(Debug, PartialEq, Eq)]
struct Timer {
    minutes: u32,
    seconds: u32,
}

impl Timer {
    /// Convert excess seconds (i.e. more than 60) into minutes.
    fn normalize(&mut self) {
        while self.seconds > 60 {
            self.seconds -= 60;
            self.minutes += 1;
        }
    }

    /// Remove one second from the timer (if any time remains).
    pub fn tick(&mut self) {
        let has_secs = self.seconds > 0;
        let has_mins = self.minutes > 0;
        match (has_secs, has_mins) {
            (true, _) => self.seconds -= 1,
            (false, true) => {
                self.minutes -= 1;
                self.seconds = 59
            }
            (false, false) => (),
        }
        self.normalize();
    }

    /// Check if any time remains.
    fn done(&self) -> bool {
        self.minutes == 0 && self.seconds == 0
    }
}

impl Default for Timer {
    fn default() -> Self {
        Timer {
            minutes: 20,
            seconds: 0,
        }
    }
}

#[test]
fn test_normalize() {
    let mut t = Timer {
        minutes: 0,
        seconds: 90,
    };
    t.normalize();
    assert_eq!(t.seconds, 30);
    assert_eq!(t.minutes, 1);

    let mut t = Timer {
        minutes: 1,
        seconds: 0,
    };
    t.normalize();
    assert_eq!(t.seconds, 0);
    assert_eq!(t.minutes, 1);

    let mut t = Timer {
        minutes: 1,
        seconds: 1,
    };
    t.normalize();
    assert_eq!(t.seconds, 1);
    assert_eq!(t.minutes, 1);

    let mut t = Timer {
        minutes: 1,
        seconds: 302,
    };
    t.normalize();
    assert_eq!(t.seconds, 2);
    assert_eq!(t.minutes, 6);
}

#[test]
fn test_tick() {
    let cases = vec![
        (0, 0, 0, 0),
        (0, 1, 0, 0),
        (1, 0, 0, 59),
        (1, 32, 1, 31),
        (2, 0, 1, 59),
    ];
    for (b_m, b_s, a_m, a_s) in cases {
        let mut t = Timer {
            minutes: b_m,
            seconds: b_s,
        };
        t.tick();
        println!("expect {}:{} -> {}:{}", b_m, b_s, a_m, a_s);
        assert_eq!(t.minutes, a_m);
        assert_eq!(t.seconds, a_s);
    }
}

/// Send a notification using `notify-send`
#[cfg(target_os = "linux")]
fn notify() {
    use std::process::Command;
    Command::new("notify-send")
        .arg("--expire-time=10000")
        .arg("Bringalingaling!")
        .arg("The timer has elapsed")
        .spawn()
        .expect("Timer couldn't execute the notifier");
}

/// Send a notification using `msg`
#[cfg(target_os = "windows")]
fn notify() {
    use std::process::Command;
    use std::env;
    let current_user = env::var("username").expect("Couldn't retrieve current user");
    Command::new("msg")
        .arg(current_user)
        .arg("The timer has elapsed")
        .spawn()
        .expect("Timer couldn't execute msg");
}

/// Try to parse a time from a string (in the format "minutes:seconds")
fn parse_time(src: &str) -> Result<Timer, &'static str> {
    let raw_parts: Vec<&str> = src.split(':').collect();
    let parts = match raw_parts.len() {
        1 => vec![raw_parts[0], "00"],
        2 => vec![raw_parts[0], raw_parts[1]],
        _ => return Err("Expected a time in the format 'mm[:ss]'"),
    };
    let minutes: u32 = match parts[0].parse::<u32>() {
        Ok(m) => m,
        Err(_) => return Err("Minutes need to be numbers"),
    };
    let seconds: u32 = match parts[1].parse::<u32>() {
        Ok(s) => s,
        Err(_) => return Err("Seconds need to be numbers"),
    };
    Ok(Timer { minutes, seconds })
}

#[test]
fn test_parse_time() {
    let cases = vec![
        ("1:1", (1, 1)),
        ("1:0", (1, 0)),
        ("0:1", (0, 1)),
        ("1:2", (1, 2)),
        ("2", (2, 0)),
    ];

    for (src, (minutes, seconds)) in cases {
        let t = parse_time(&src).unwrap();
        assert_eq!(t, Timer { minutes, seconds });
    }
}

fn get_timer() -> Result<Timer, &'static str> {
    use std::env;
    let argv: Vec<String> = env::args().into_iter().collect();
    match argv.len() {
        0 => Ok(Timer::default()),
        1 => Ok(Timer::default()),
        2 => parse_time(&argv[1]),
        _ => Err("Too many arguments."),
    }
}

fn main() {
    use std::io::{self, Write};
    let mut t = get_timer().expect("Error parsing timer");
    let dt = std::time::Duration::from_millis(1000);
    print!("\x1B[2J\x1B[0;0H"); // clear screen, set position to 0, 0
    println!("Counting down");
    while !t.done() {
        t.tick();
        print!("\r\x1B[K{}:{:02}", t.minutes, t.seconds);
        io::stdout().flush().unwrap();
        std::thread::sleep(dt);
    }
    println!("\nDone!");
    notify();
}
